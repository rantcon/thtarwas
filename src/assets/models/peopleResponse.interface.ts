export interface PeopleResponse {
    name: string;
    height: string;
    mass: string;
    hair_color: string;
    skin_color: string;
    eye_color: string;
    birth_year: string;
    gender: string;
    homeworld: string;
    films: string[];
    species: [];
    starships: [];
    vehicles: [];
    created: Date;
    edited: Date;
    url: string;
}