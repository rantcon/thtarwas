import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PeopleResponse } from 'src/assets/models/peopleResponse.interface';
import { PeopleService } from '../services/people/people.service';

@Component({
  selector: 'app-searchlist',
  templateUrl: './searchlist.component.html',
  styleUrls: ['./searchlist.component.scss']
})
export class SearchlistComponent implements OnInit {

  @ViewChild('search') searchElement: ElementRef;

  @Output() 
  select: EventEmitter<string> = new EventEmitter<string>();

  currentPage: number = 1;
  pageInformation: {
    count: number;
    next: string;
    pages: number;
    prev: string;
  } = {
    count: 0,
    next: '',
    pages: 0,
    prev: ''
  }

  people: PeopleResponse[];
  clonePeople: PeopleResponse[];
  searchForm: FormGroup;

  swapi: string = 'https://swapi.dev/api/people/'

  constructor(
    private peopleService: PeopleService,
    private formBuilder: FormBuilder
  ) {
    this.searchForm = this.formBuilder.group({
      name: '',
    });

    this.onChanges();
  }

  onChanges() {
    this.searchForm.valueChanges.subscribe(val => {
      let filtered = this.clonePeople.filter(char => {
        return char.name.toLowerCase().indexOf(val.name) > -1
      });
      this.people = filtered;
    })
  }

  ngOnInit(): void {
    this.getPeople();
  }

  getPeople() {
    this.peopleService.getPeople(this.swapi).subscribe(people => {
      this.populatePage(people);
    })
  }

  populatePage(res) {
    console.log('populate: ', res);
    this.pageInformation.count = res.count;
    this.pageInformation.next = res.next;
    this.pageInformation.prev = res.previous;
    this.people = res.results;
    this.clonePeople = JSON.parse(JSON.stringify(res.results));
    setTimeout(() => {
      this.searchElement.nativeElement.focus();
    }, 100);
  }

  nextPage() {
    this.peopleService.getPeople(this.pageInformation.next).subscribe(people => {
      this.currentPage +=1;
      this.populatePage(people);
    })
  }

  prevPage() {
    this.peopleService.getPeople(this.pageInformation.prev).subscribe(people => {
      this.currentPage -=1;
      this.populatePage(people);
    })
  }

  selectCharacter(char) {
    this.select.emit(char);
  }


}
