import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of, Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { PeopleResponse } from '../../../assets/models/peopleResponse.interface';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {


  maxPeople: number = 84;
  messages: string[] = [];

  constructor(
    private http: HttpClient
  ) { }

  getRandomPerson(): Observable<PeopleResponse> {
    this.log('getRandomPerson()');

    let timestamp = new Date().getUTCMilliseconds();
    let randomPerson = timestamp % this.maxPeople;
    let url = `https://swapi.dev/api/people/${randomPerson}`;

    return this.http.get<PeopleResponse>(url).pipe(
      tap(_ => this.log('getRandomPerson() complete')),
      catchError(this.handleError<PeopleResponse>('getRandomPerson'))
    );
  }

  getPeople(url): Observable<PeopleResponse[]> {
    this.log('getPeople()');
    return this.http.get<PeopleResponse[]>(url).pipe(
      tap(_ => this.log('getPeople() complete')),
      catchError(this.handleError<PeopleResponse[]>('getPeople'))
    );
  }

  private log(message: string) {
    this.add(`HeroService: ${message}`);
  }

  public handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    }
  }

  add(message: string) {
    this.messages.push(message);
  }

  clear() {
    this.messages = [];
  }
}
