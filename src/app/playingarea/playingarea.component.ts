import { Component, OnInit } from '@angular/core';
import { PeopleResponse } from 'src/assets/models/peopleResponse.interface';
import { PeopleService } from '../services/people/people.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-playingarea',
  templateUrl: './playingarea.component.html',
  styleUrls: ['./playingarea.component.scss']
})
export class PlayingareaComponent implements OnInit {

  player1Name: string;
  player2Name: string;

  player1Card: PeopleResponse;
  player2Card: PeopleResponse;

  player1Locked: boolean = false;
  player2Locked: boolean = false;

  p1points: number = 0;
  p2points: number = 0;

  player1search: boolean = false;
  player2search: boolean = false;

  showWinner: boolean = false;

  constructor(
    private peopleService: PeopleService
  ) { }

  ngOnInit(): void {
  }

  getCards() {

  }

  saveP1(player1Name) {
    this.player1Name = player1Name;
    this.peopleService.getRandomPerson().subscribe(card => {
      this.player1Card = card;
    })
  }

  saveP2(player2Name) {
    this.player2Name = player2Name;
    this.peopleService.getRandomPerson().subscribe(card => {
      this.player2Card = card;
    })
  }

  setNewCard(ev, player) {
    if (player == 1) {
      this.player1Card = ev;
    } else if (player == 2) {
      this.player2Card = ev;
    }
  }

  compareCards(ev, player) {
    console.log('compareCards()')
    if (player == 1) {
      this.player1Locked = true;
    } else if (player == 2) {
      this.player2Locked = true;
    }

    if (this.player1Locked && this.player2Locked) {
      this.topTrumps();
    }
  }

  searchList(ev, player) {
    console.log('searchlist()');
    this.switchToSearch(player);
  }

  switchToSearch(player) {
    if (player == 1) {
      this.player1search = true;
    } else if (player == 2) {
      this.player2search = true;
    }
  }

  foundCharacter(ev, player) {
    console.log('Found Char: ', ev);
    if (player == 1) {
      this.player1Card = ev;
      this.player1search = false;
    } else if (player == 2) {
      this.player2Card = ev;
      this.player2search = false;
    }

  }

  topTrumps() {
    console.log('topTrumps()');
    this.p1points = 0;
    this.p2points = 0;

    if (this.player1Card.height > this.player2Card.height) {
      this.p1points += 1;
    } else {
      this.p2points += 1;
    }

    if (this.player1Card.mass > this.player2Card.mass) {
      this.p1points += 1;
    } else {
      this.p2points += 1;
    }

    if (this.player1Card.films.length > this.player2Card.films.length) {
      this.p1points += 1;
    } else {
      this.p2points += 1;
    }

    if (this.player1Card.starships.length > this.player2Card.starships.length) {
      this.p1points += 1;
    } else {
      this.p2points += 1;
    }

    if (this.player1Card.vehicles.length > this.player2Card.vehicles.length) {
      this.p1points += 1;
    } else {
      this.p2points += 1;
    }

    this.showWinner = true;
    if (this.p1points > this.p2points) {
      Swal.fire({
        title: `${this.player1Name}`,
        text: 'Is the winner!',
        icon: 'success',
        confirmButtonText: 'Done!'
      }).then(res => {
        this.reset();
      })
    } else {
      Swal.fire({
        title: `${this.player2Name}`,
        text: 'Is the winner!',
        icon: 'success',
        confirmButtonText: 'Done!'
      }).then(res => {
        this.reset();
      })
    }
  }

  reset() {
    this.player1Name = undefined;
    this.player2Name = undefined;

    this.player1Card = undefined;
    this.player2Card = undefined;

    this.player1Locked = false;
    this.player2Locked = false;

    this.p1points = 0;
    this.p2points = 0;

    this.showWinner = false;
  }

}
