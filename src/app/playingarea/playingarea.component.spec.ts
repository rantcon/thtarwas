import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayingareaComponent } from './playingarea.component';

describe('PlayingareaComponent', () => {
  let component: PlayingareaComponent;
  let fixture: ComponentFixture<PlayingareaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayingareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayingareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
