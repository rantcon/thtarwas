import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrumpcardComponent } from './trumpcard.component';

describe('TrumpcardComponent', () => {
  let component: TrumpcardComponent;
  let fixture: ComponentFixture<TrumpcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrumpcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrumpcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
