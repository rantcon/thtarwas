import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PeopleResponse } from 'src/assets/models/peopleResponse.interface';
import { PeopleService } from '../services/people/people.service';

@Component({
  selector: 'app-trumpcard',
  templateUrl: './trumpcard.component.html',
  styleUrls: ['./trumpcard.component.scss']
})
export class TrumpcardComponent implements OnInit {

  lockedIn: boolean = false;

  @Input() playingCard: PeopleResponse;
  @Input() playerName: PeopleResponse;
  
  @Output() 
  cardChange: EventEmitter<PeopleResponse> = new EventEmitter<PeopleResponse>();

  @Output() 
  compare: EventEmitter<string> = new EventEmitter<string>();

  @Output() 
  search: EventEmitter<string> = new EventEmitter<string>();

  constructor(
    private peopleService: PeopleService
  ) { }

  ngOnInit(): void {
    // this.getTrumpCard();
  }

  getTrumpCard() {
    this.peopleService.getRandomPerson().subscribe(card => {
      console.log('Card: ', card);
      this.playingCard = card;
    })
  }

  newCard() {
    this.peopleService.getRandomPerson().subscribe(card => {
      console.log('emit: ', card);
      this.cardChange.emit(card);
    })
  }

  compareCards() {
    this.lockedIn = true;
    this.compare.emit('compare cards!');
  }

  searchPeople() {
    this.search.emit('search page!');
  }

}
